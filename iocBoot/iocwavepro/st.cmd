#!../../bin/linux-x86_64/wavepro

#- You may have to change wavepro to something else
#- everywhere it appears in this file

< envPaths

## Register all support components
dbLoadDatabase("$(TOP)/dbd/wavepro.dbd",0,0)
wavepro_registerRecordDeviceDriver(pdbbase) 

epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "SCP-02")
epicsEnvSet("DEVICE_IP", "bd-scp02.cslab.esss.lu.se")


epicsEnvSet("PREFIX", "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("PORT",   "$(PREFIX)-asyn-port")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(TOP)/db")
# Report streamdevice errors
var streamError 1

# Connect to instrument via VXI11
vxi11Configure("$(PORT)", "$(DEVICE_IP)", 0, "0.0", "inst0", 0)

## Load record instances
dbLoadRecords("$(TOP)/db/wavepro.db", "P=$(PREFIX),R=,PORT=$(PORT)")

iocInit()
